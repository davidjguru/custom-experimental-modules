<?php

namespace Drupal\Tests\humanstxt\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic functionality of configured humans.txt files.
 *
 * @group Humans.txt
 */
class HumansTxtBasicTest extends BrowserTestBase {

  /**
   * Provides the default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['humanstxt', 'humanstxt_test'];

  /**
   * User with proper permissions for module configuration.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * User with content access.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $normalUser;

  /**
   * Checks if an admin user can access to the configuration page.
   */
  public function testHumansTxtAdminAccess() {
    // Create an user with humanstxt permissions.
    $this->adminUser = $this->drupalCreateUser(['administer humans.txt']);
    // Login for the former admin user.
    $this->drupalLogin($this->adminUser);
    // Access to the path of humanstxt config page.
    // Requires path, not route.
    // @see UiHelperTrait::drupalGet
    $this->drupalGet('admin/config/development/humanstxt');
    // Check the response returned by Drupal.
    $this->assertResponse(200);
  }

  /**
   * Checks if a non-administrative user cannot access to the config page.
   */
  public function testHumansTxtUserNoAccess() {
    // Create a basic user without specific permissions.
    $this->normalUser = $this->drupalCreateUser(['access content']);
    // Login for the former basic user.
    $this->drupalLogin($this->normalUser);
    // Try access to the path of humanstxt config page.
    // Requires path, not route.
    // @see https://api.drupal.org/api/drupal/core%21tests%21Drupal%21Tests%21UiHelperTrait.php/function/UiHelperTrait%3A%3AdrupalGet/8.8.x
    $this->drupalGet('admin/config/development/humanstxt');
    // Check the response returned by Drupal.
    $this->assertResponse(403);
  }

}

/**
 * @file
 * Contains local JS for embedding the powerbi data.
 */

(function($, Drupal) {
    Drupal.behaviors.powerBIJavaScript = {
        attach: function(context, settings) {
    let viewMode = "view";
    let typeToken = settings.type;
    let tokenValue = settings.token;
    let urlEmbed = settings.url;
    let idReport = settings.id;

    let models = window['powerbi-client'].models;
    let permissions = models.Permissions.All;
    console.log("Token", tokenValue);
    
    let config = {
      type: 'report', 
      tokenType: typeToken,
      accessToken: tokenValue,
      embedUrl: urlEmbed, 
      id: idReport,
      permissions: permissions,
      settings: {
        panes: {
          filters: {
            visible: true
            },
           pageNavigation: {
             visible: true
              }
          }
       }
    };

    // Get a reference to the embedded report HTML element
    var embedContainer = $('#embedContainer')[0];
    
    // Embed the report and display it within the div container.
    var report = powerbi.embed(embedContainer, config);
    
    }
   };
}(jQuery, Drupal));


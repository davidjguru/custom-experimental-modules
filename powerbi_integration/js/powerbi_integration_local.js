/**
 * @file
 * Contains local JS for embedding the powerbi data.
 */

(function($) {
    Drupal.behaviors.powerBIPHP = {
        attach: function(context, settings) {
            for (var i = 0; i < settings.powerBI.value.length; i++) {
                var embedConfiguration = {
                    // Like 'dashboard', report etc.
                    type: settings.component,
                    id: settings.powerBI.value[i].id,
                    embedUrl: settings.powerBI.value[i].embedUrl,
                    accessToken: settings.accessToken,
                };
                var $reportContainer = $('#data-tab-' + i);
                powerbi.embed($reportContainer.get(0), embedConfiguration);
            }

            // Enable default tab.
            $('#tabs li a:not(:first)').addClass('inactive');
            $('.tabcontent').hide();
            $('.tabcontent:first').show();

            $('#tabs li a').click(function() {
                var t = $(this).attr('id');
                if ($(this).hasClass('inactive')) {
                    $('#tabs li a').addClass('inactive');
                    $(this).removeClass('inactive');
                    $('.tabcontent').hide();
                    $('#data-' + t).fadeIn('slow');
                }
            });
        }
    };
}(jQuery));

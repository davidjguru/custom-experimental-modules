<?php

namespace Drupal\powerbi_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\powerbi\Client\PowerbiIntegrationClient;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PowerbiIntegrationAdvancedController.
 *
 * @package Drupal\powerbi_integration\Controller
 */
class PowerbiIntegrationAdvancedController extends ControllerBase {

  /**
   * The PowerbiIntegrationClient API.
   *
   * @var PowerbiIntegrationClient
   */
  protected $powerbiApiClient;

  /**
   * PowerbiIntegration module 'powerbi_integration.settings' configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $powerbiIntegrationConfig;


  /**
   * PowerbiIntegration username.
   *
   * @var string
   */
  protected $username;

  /**
   * PowerbiIntegration password.
   *
   * @var string
   */
  protected $password;

  /**
   * PowerbiIntegration $clientId.
   *
   * @var string
   */
  protected $clientId;

  /**
   * PowerbiIntegration $groupId.
   *
   * @var string
   */
  protected $groupId;

  /**
   * The powerbiIntegration $urlResource.
   *
   * @var string
   */
  protected $urlResource;

  /**
   * The powerbiIntegration $urlLogin.
   *
   * @var string
   */
  protected $urlLogin;

  /**
   * The powerbiIntegration $urlComponent.
   *
   * @var string
   */
  protected $urlComponent;
  
  /**
   * PowerbiController constructor.
   *
   * @param PowerbiIntegrationClient $powerbi_api_client
   *   The powerbi api client.
   */
  public function __construct(PowerbiIntegrationClient $powerbi_api_client) {
    $this->powerbiApiClient = $powerbi_api_client;
    $this->powerbiIntegrationConfig = $this->config('powerbi.settings');

    $this->username = $this->powerbiIntegrationConfig->get('powerbi_integration_username');
    $this->password = $this->powerbiIntegrationConfig->get('powerbi_integration_password');
    $this->clientId = $this->powerbiIntegrationConfig->get('powerbi_integration_client_id');
    $this->groupId = $this->powerbiIntegrationConfig->get('powerbi_integration_group_id');
    $this->urlResource = $this->powerbiIntegrationConfig->get('powerbi_integration_url_resource');
    $this->urlLogin = $this->powerbiIntegrationConfig->get('powerbi_integration_url_login');
    $this->urlComponent = $this->powerbiIntegrationConfig->get('powerbi_integration_url_component');

  }

  /**
   * Service container to create service instance.
   *
   * @param ContainerInterface $container
   *   The service container.
   *
   * @return ControllerBase|static
   *   Services.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('powerbi_integration.client')
    );
  }

  /**
   * Display the powerbi dashboard.
   *
   * @param mixed $component
   *   Component to embed.
   *
   * @return array|mixed
   *   The data to be embedded on the page.
   */
  public function getRender($component) {

    $token = isset($_COOKIE["access_token"]) ? $_COOKIE["access_token"] : $this->getAuthToken();
    $endpoint = $this->urlComponent . $this->groupId . '/' . $component;
    $request = $this->powerbiApiClient->connect('get', $endpoint, $token, []);
    $results = json_decode($request, TRUE);
    
    $data = [
      'powerBI' => $results,
      'accessToken' => $token,
      'component' => substr_replace($component, "", -1),
    ];
    
    return [
      '#theme' => 'getting-powerbi-data-local',
      '#data' => $results,
      '#attached' => [
        'library' => [
          'powerbi_integration/powerbi_integration_local',
          'powerbi_integration/powerbi-client',
        ],
        'drupalSettings' => $data,
      ],
    ];
  }

  /**
   * Get Title of the Component.
   *
   * @param mixed $component
   *   Component like page, report etc.
   *
   * @return mixed
   *   Component.
   */
  public function getTitle($component) {
    return $component;
  }

  /**
   * Get authorization token.
   *
   * @return mixed|null
   *   Authorization token.
   */
  public function getAuthToken() {

    if (isset($_COOKIE["access_token"])) {
      return $_COOKIE["access_token"];
    }

    $body = [
      'grant_type' => 'password',
      'scope' => 'openid',
      'resource' => $this->urlResource,
      'client_id' => $this->clientId,
      'username' => $this->username,
      'password' => $this->password,
    ];
    $endpoint = $this->urlLogin;
    $request = $this->powerbiApiClient->connect('post', $endpoint, NULL, $body);
    $results = json_decode($request, TRUE);
    
    if (!empty($results)) {
      setcookie("access_token", $results['access_token'], $results['expires_on']);
      return $results['access_token'];
    }
    return NULL;
  }

}

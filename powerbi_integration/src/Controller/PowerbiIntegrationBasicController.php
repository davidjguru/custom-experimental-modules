<?php


namespace Drupal\powerbi_integration\Controller;

use Drupal\Core\Controller\ControllerBase;


class PowerbiIntegrationBasicController extends ControllerBase {

  protected $powerbiIntegrationConfig;
  
  public function getRender() {
    
    $this->powerbiIntegrationConfig = $this->config('powerbi_integration.settings');
    
    $data = [
      'view' => $this->powerbiIntegrationConfig->get('powerbi_integration_view'),
      'type' => $this->powerbiIntegrationConfig->get('powerbi_integration_type'),
      'token' => $this->powerbiIntegrationConfig->get('powerbi_integration_token'),
      'url' => $this->powerbiIntegrationConfig->get('powerbi_integration_url'),
      'id' => $this->powerbiIntegrationConfig->get('powerbi_integration_id'),
    ];
    
    return [
      '#title' => t('PowerBI Integration View Example with JavaScript'),
      '#theme' => 'getting-powerbi-data',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'powerbi_integration/powerbi_integration',
          'powerbi_integration/powerbi-client',
        ],
        'drupalSettings' => $data,
      ],
    ];
  }
}
<?php

namespace Drupal\powerbi_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Declaration of class PowerbiIntegrationSettingsForm.
 */
class PowerbiIntegrationAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'powerbi_integration_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'powerbi_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Default settings.
    $config = $this->config('powerbi_integration.settings');
    $form['settings'] = ['#type' => 'vertical_tabs'];
    
    // Mode to embed tab.
    $form['mode']['embed'] = [
      '#type' => 'fieldset',
      '#title' => t('EMBED MODE: SELECT MAIN OPTIONS FOR RENDERING'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
      ];
    
    $options = ['View mode', 'Edit mode', 'Create mode'];
    $options_two = ['Embed token', 'AAD token'];

    $form['mode']['embed']['view'] = [
      '#type' => 'radios',
      '#title' => t('View mode'),
      '#options' => $options,
      '#default_value' => $config->get('powerbi_integration_view'),
      '#description' => t('Select option for the embed format'),
    ];

    $form['mode']['embed']['type'] = [
      '#type' => 'radios',
      '#title' => t('Token Type'),
      '#options' => $options_two,
      '#default_value' => $config->get('powerbi_integration_type'),
      '#description' => t('Select option for the Token Type'),
    ];

    $form['mode']['data'] = [
      '#type' => 'fieldset',
      '#title' => t('DATA FOR LIGHTWEIGHT CONNECTIONS WITH JAVASCRIPT'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    ];

    $form['mode']['data']['token'] = [
      '#type' => 'textfield',
      '#title' => t('Embed Token'),
      '#default_value' => $config->get('powerbi_integration_token'),
      '#description' => t('Fill the token field to get the code to embed'),
    ];

    $form['mode']['data']['url'] = [
      '#type' => 'textfield',
      '#title' => t('Embed URL'),
      '#default_value' => $config->get('powerbi_integration_url'),
      '#description' => t('Fill the embed URL field to get the code to embed'),
    ];

    $form['mode']['data']['id'] = [
      '#type' => 'textfield',
      '#title' => t('Report ID'),
      '#default_value' => $config->get('powerbi_integration_id'),
      '#description' => t('Fill the report ID field to get the code to embed'),
    ];

    // Mode to register tab.
    $form['mode']['register'] = [
      '#type' => 'fieldset',
      '#title' => t('DATA FOR HEAVY CONNECTIONS USING LOGIN WITH PHP'),
      '#description' => t('Fill only if you wanna use REST API Connections'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    ];
    
    $form['mode']['register']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Power BI Username'),
      '#description' => $this->t('Enter your PowerBI username., Ex: john.doe@yourdomain.com.'),
      '#default_value' => $config->get('powerbi_integration_username'),
    ];
    
    $form['mode']['register']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Power BI Password'),
      '#description' => $this->t('Enter your password, saved password will not be shown here.'),
      '#default_value' => $config->get('powerbi_integration_password'),
    ];
    
    $form['mode']['register']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Power BI Application ID'),
      '#description' => $this->t('Enter your registered application ID.'),
      '#default_value' => $config->get('powerbi_integration_client_id'),
    ];
    
    $form['mode']['register']['group_id']= [
      '#type' => 'textfield',
      '#title' => $this->t('Power BI Group ID'),
      '#description' => $this->t('Enter your group ID.'),
      '#default_value' => $config->get('powerbi_integration_group_id'),
    ];

    $form['mode']['register']['url_resource']= [
      '#type' => 'textfield',
      '#title' => $this->t('URL of the Resource'),
      '#description' => $this->t('Enter the URL for the resource location.'),
      '#default_value' => $config->get('powerbi_integration_url_resource'),
    ];

    $form['mode']['register']['url_login']= [
      '#type' => 'textfield',
      '#title' => $this->t('URL for login'),
      '#description' => $this->t('Enter the URL for login.'),
      '#default_value' => $config->get('powerbi_integration_url_login'),
    ];

    $form['mode']['register']['url_component']= [
      '#type' => 'textfield',
      '#title' => $this->t('URL for the component'),
      '#description' => $this->t('Enter the URL of your component.'),
      '#default_value' => $config->get('powerbi_integration_url_component'),
    ];
    

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->config('powerbi_integration.settings')
    ->set('powerbi_integration_view', $form_state->getValue('view'))
    ->set('powerbi_integration_type', $form_state->getValue('type'))
    ->set('powerbi_integration_token', $form_state->getValue('token'))
    ->set('powerbi_integration_url', $form_state->getValue('url'))
    ->set('powerbi_integration_id', $form_state->getValue('id'))
    ->set('powerbi_integration_username', $form_state->getValue('username'))
    ->set('powerbi_integration_password', $form_state->getValue('password'))
    ->set('powerbi_integration_client_id', $form_state->getValue('client_id'))
    ->set('powerbi_integration_group_id', $form_state->getValue('group_id'))
    ->set('powerbi_integration_url_resource', $form_state->getValue('url_resource'))
    ->set('powerbi_integration_url_login', $form_state->getValue('url_login'))
    ->set('powerbi_integration_url_component', $form_state->getValue('url_component'))
    ->save();

    return parent::submitForm($form, $form_state);
  }

}
